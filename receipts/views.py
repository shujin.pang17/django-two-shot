from django.shortcuts import redirect, render, get_object_or_404, reverse
from receipts.forms import AccountForm, ExpenseCategoryForm, ReceiptForm
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipts,
    }
    return render(request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = (request.user)
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)

@login_required
def expensecategory_list(request):
    expensecategories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": expensecategories,
    }
    return render(request, "receipts/categories/list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = (request.user)
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }

    return render(request, "receipts/categories/create.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "account_list": accounts,
    }
    return render(request, "receipts/accounts/list.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = (request.user)
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }

    return render(request, "receipts/accounts/create.html", context)
